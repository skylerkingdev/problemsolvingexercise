using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UIToolkit.UI;


public class SecondView : UIView {
 
	public UIList list;

	void Start ()
	{
		List<string> data = new List<string>();

		for (int i=1; i<1000; i++)
		{
			data.Add (i + "00000000");
		}

		list.Populate(new ArrayList(data));
	}

//	public override void ViewWillAppear (UIView sourceView = null) {
//		base.ViewWillAppear (sourceView);
//
//		 
//		//DO NOT MODIFY THE DATA
//		//cell data
//		//List<long> data = new List<long>();
//		//for (int i=0; i<1000; i++) data.Add(i * 100000000l);
//		//
//		//list.Populate(new ArrayList(data));
//
//
//		//youre reaching the ceiling of int by doing such a massive calculation
//		//one solution would be to pad the value with 0's and write it utilizing a string
//		//you also need to start iterating at 1 to avoid the error in the first entry
		//you can also speed this up by populating the list once only in start or something one shot like ViewDidLoad
//		List<string> data = new List<string>();
//
//		for (int i=1; i<1000; i++)
//		{
//			data.Add (i + "00000000");
//		}
//
//		list.Populate(new ArrayList(data));
//
//		Debug.Log ("viewwillappear");
//
//
//
//	}


		
}